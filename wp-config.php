<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cKIl6MPIMZvX0KIiXwWkKcSZCoRzMwIKyOoo9RKh1WCzlLxh2zDmAuL2q62jvmv0ZGzkER6WsK/CUfiM/9lL2w==');
define('SECURE_AUTH_KEY',  '+elhKfI1QnqPTH9lO4MrlQZkR821WZe3EHcvQNUbTUZ5+9mDKV2cnD/1hXxFchCwg0NLNX7L1u9/7rcsmNjhaw==');
define('LOGGED_IN_KEY',    'ae203Cinv4GrUrlqc3Xf5PNIQs4orS4C5sTYvrrhPDdRw9ptZaFRQ9QeRKt30zl9ojXl6TpueNcCeouo85L7LA==');
define('NONCE_KEY',        'wbWOU18MT0THl1HFt4y3VTTe0DklM/0fonToC1d8D8P4WYkFAlG7mKXVM1VYdo78RGUsGwftAFN9Ik8zFeSXrQ==');
define('AUTH_SALT',        'mKrrudtP86LihYgWxYQMVqH2yoTfwSwth3UBwJZwIug23uqNkm4Uj9UWRH3eesUEeD5oAdgdEVOxV7bNcHu8XQ==');
define('SECURE_AUTH_SALT', 'vVPNdYGa/bvQTktXoC2j4htJd29y7B0wnMYaX0/MU80qQuoRjKnPPqm3AjNX2TzxTcnMPIRjqkLVw9OalkhErQ==');
define('LOGGED_IN_SALT',   'EhnBjHeZjW89NxuyfNPHq0yARWgiz2wevhlaBHdoJGbE4pm3XRVEpF5qAiIe8ICNsJiuPR8CTqXIITi343Dnnw==');
define('NONCE_SALT',       'FonwQM8FnuM5hozjXWI3yVPYFIyg9Kr0xCmjwOdDk6AUZDbYLtE2ECLyjiifxs5D6ZaJFO+bwcLrv/UCwl06UA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

/* Inserted by Local by Flywheel. Fixes $is_nginx global for rewrites. */
if (strpos($_SERVER['SERVER_SOFTWARE'], 'Flywheel/') !== false) {
	$_SERVER['SERVER_SOFTWARE'] = 'nginx/1.10.1';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
