<?php
/*
Plugin Name: MVF Videos for Wordpress
Plugin URI:  https://github.com/dmbostan/mvf-videos
Description: MVF WordPress Test
Version:     0.0.1
Author:      Dmitri Bostan
Author URI:  https://dmbostan.github.io/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: mvf
*/

/*--------------------------------------------------------------
	Exit if accessed directly
--------------------------------------------------------------*/

if ( ! defined( 'ABSPATH' ) ) {
	die( 'No script kiddies please!' );
}

/*--------------------------------------------------------------
	Define constants
--------------------------------------------------------------*/
define( 'MVF_PLUGIN_DIR', plugin_dir_url(__FILE__) );

/*--------------------------------------------------------------
	Initialize WordPress menu page
--------------------------------------------------------------*/

require_once( dirname( __FILE__ ) . '/includes/classes/class.MVFPostType.inc.php' );
require_once( dirname( __FILE__ ) . '/includes/classes/class.MVFMetaBox.inc.php' );
require_once( dirname( __FILE__ ) . '/includes/classes/class.MVFShortcodes.inc.php' );
require_once( dirname( __FILE__ ) . '/includes/custom-args.php' );
require_once( dirname( __FILE__ ) . '/includes/init.php' );


