(function () {
    tinymce.PluginManager.add('mvf_video', function (editor, url) {
        //console.log(tinyMCE_object.video_list);
        var obj = tinyMCE_object.video_list,
            video_list = [];
        Object.keys(obj).forEach(function (key) {
            var video_object = {text: obj[key], value: key};
            video_list.push(video_object);
        });
        console.log(video_list);

        editor.addButton('mvf_video', {
            text: tinyMCE_object.button_name,
            icon: false,
            onclick: function () {
                editor.windowManager.open({
                    title: tinyMCE_object.button_title,
                    body: [
                        {
                            type: 'listbox',
                            name: 'mvf_video_id',
                            label: tinyMCE_object.image_title,
                            values: video_list
                        },
                        {
                            type: 'textbox',
                            subtype: 'number',
                            min: '0',
                            name: 'mvf_video_border_width',
                            label: tinyMCE_object.border_width_title,
                            value: '8'
                       },
                        {
                            type: 'colorpicker',
                            name: 'mvf_video_border_color',
                            label: tinyMCE_object.border_color_title,
                            value: '#333333'
                        }
                    ],
                    onsubmit: function (e) {
                        editor.insertContent('[mvf_video id="' + e.data.mvf_video_id + '" border_color="' + e.data.mvf_video_border_color + '" border_width="' + e.data.mvf_video_border_width + '"]');
                    }
                });
            },
        });
    });
})();