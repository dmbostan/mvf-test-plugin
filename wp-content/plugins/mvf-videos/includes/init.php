<?php
$mvf_videos = new MVF_Post_Type(
	'Video',
	'Videos',
	$mfv_video_args );

$mvf_meta_boxes = new MVF_Metabox(
	$mvf_videos->get_id(),
	$mvf_videos->get_id() . 'metabox',
	'Video details',
	'advanced',
	'core',
	$mvf_video_fields
);

$mvf_shortcodes = new MVF_Video_Shortcode( $mvf_videos->get_id() );