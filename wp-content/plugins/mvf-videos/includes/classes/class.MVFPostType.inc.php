<?php

/*--------------------------------------------------------------
	Exit if accessed directly
--------------------------------------------------------------*/

if ( ! defined( 'ABSPATH' ) ) {
	die( 'No script kiddies please!' );
}

/*--------------------------------------------------------------
	The mighty MVF_Videos class
--------------------------------------------------------------*/

if ( ! class_exists( 'MVF_Post_Type' ) ) {
	class MVF_Post_Type {

		/* Class constructor */
		public function __construct( $name_singular, $name_plural, $args = array(), $labels = array() ) {
			// Set some important variables
			$this->post_type_id            = strtolower( str_replace( ' ', '_', 'mvf_' . $name_singular ) );
			$this->post_type_name_singular = sanitize_text_field( $name_singular );
			$this->post_type_name_plural   = sanitize_text_field( $name_singular );
			$this->post_type_args          = $args;
			$this->post_type_labels        = $labels;

			if ( ! post_type_exists( $this->post_type_id ) ) {
				add_action( 'init', array( &$this, 'register_post_type' ) );
			}

		}

		public function get_id() {
			return $this->post_type_id;
		}

		/* Method which registers the post type */
		public function register_post_type() {

			$labels = array_merge(
				array(
					'name'                  => _x( $this->post_type_name_plural, 'Post Type General Name', 'mvf' ),
					'singular_name'         => _x( $this->post_type_name_singular . 'aaeeeee', 'Post Type Singular Name', 'mvf' ),
					'menu_name'             => __( $this->post_type_name_plural, 'mvf' ),
					'name_admin_bar'        => __( $this->post_type_name_plural, 'mvf' ),
					'archives'              => __( 'Item Archives', 'mvf' ),
					'attributes'            => __( 'Item Attributes', 'mvf' ),
					'parent_item_colon'     => __( 'Parent Item:', 'mvf' ),
					'all_items'             => __( 'All ' . strtolower( $this->post_type_name_plural ), 'mvf' ),
					'add_new_item'          => __( 'Add New ' . $this->post_type_name_singular, 'mvf' ),
					'add_new'               => __( 'Add New', 'mvf' ),
					'new_item'              => __( 'New ' . strtolower( $this->post_type_name_singular ), 'mvf' ),
					'edit_item'             => __( 'Edit ' . strtolower( $this->post_type_name_singular ), 'mvf' ),
					'update_item'           => __( 'Update ' . strtolower( $this->post_type_name_singular ), 'mvf' ),
					'view_item'             => __( 'View ' . strtolower( $this->post_type_name_singular ), 'mvf' ),
					'view_items'            => __( 'View ' . strtolower( $this->post_type_name_plural ), 'mvf' ),
					'search_items'          => __( 'Search ' . strtolower( $this->post_type_name_singular ), 'mvf' ),
					'not_found'             => __( 'Not found', 'mvf' ),
					'not_found_in_trash'    => __( 'Not found in Trash', 'mvf' ),
					'featured_image'        => __( 'Featured Image', 'mvf' ),
					'set_featured_image'    => __( 'Set featured image', 'mvf' ),
					'remove_featured_image' => __( 'Remove featured image', 'mvf' ),
					'use_featured_image'    => __( 'Use as featured image', 'mvf' ),
					'insert_into_item'      => __( 'Insert into item', 'mvf' ),
					'uploaded_to_this_item' => __( 'Uploaded to this ' . strtolower( $this->post_type_name_singular ), 'mvf' ),
					'items_list'            => __( $this->post_type_name_plural . ' list', 'mvf' ),
					'items_list_navigation' => __( strtolower( $this->post_type_name_plural ) . ' list navigation', 'mvf' ),
					'filter_items_list'     => __( 'Filter ' . strtolower( $this->post_type_name_plural ) . ' list', 'mvf' ),
				),

				$this->post_type_labels

			);

			$args = array_merge(

				array(
					'label'               => __( $this->post_type_name_plural, 'mvf' ),
					'labels'              => $labels,
					'supports'            => false,
					'taxonomies'          => array(),
					'hierarchical'        => false,
					'public'              => true,
					'show_ui'             => true,
					'show_in_menu'        => true,
					'menu_position'       => 5,
					'show_in_admin_bar'   => true,
					'show_in_nav_menus'   => false,
					'can_export'          => false,
					'has_archive'         => false,
					'exclude_from_search' => false,
					'publicly_queryable'  => false,
					'capability_type'     => 'post'
				),

				$this->post_type_args

			);

			register_post_type( $this->post_type_id, $args );
		}
	}
}
