<?php

/**
 * Class MVF_Video_Shortcode
 */
if ( ! class_exists( 'MVF_Video_Shortcode' ) ) {
	class MVF_Video_Shortcode {

		private $css_output, $the_css;

		public function __construct( $post_type ) {


			$this->post_type = $post_type;

			add_action( 'after_setup_theme', array( $this, 'mvf_shortcodes_setup' ) );
			add_filter( 'mce_external_plugins', array( $this, 'mvf_add_buttons' ) );
			add_filter( 'mce_buttons', array( $this, 'mvf_register_buttons' ) );
			add_action( 'after_wp_tiny_mce', array( $this, 'mvf_tinymce_extra_vars' ) );
			add_shortcode( 'mvf_video', array( $this, 'parse_shortcode' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_assets' ) );


		}

		public function mvf_shortcodes_setup() {
			add_action( 'init', array( $this, 'mvf_buttons' ) );
		}

		public function mvf_buttons() {
			if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
				return;
			}

			if ( get_user_option( 'rich_editing' ) !== 'true' ) {
				return;
			}


		}

		public function mvf_add_buttons( $plugin_array ) {
			$plugin_array['mvf_video'] = MVF_PLUGIN_DIR . 'admin/js/video-button.js';

			return $plugin_array;
		}

		public function mvf_register_buttons( $buttons ) {
			array_push( $buttons, 'mvf_video' );

			return $buttons;
		}

		private function video_list() {
			$videos      = array();
			$video_query = new WP_Query( array(
				'posts_per_page' => - 1,
				'post_type'      => $this->post_type
			) );

			if ( $video_query->have_posts() ) {
				while ( $video_query->have_posts() ) {
					$video_query->the_post();
					$videos[ get_the_ID() ] = get_the_title();
				}
				wp_reset_postdata();
			}

			return $videos;
		}

		public function mvf_tinymce_extra_vars() { ?>
            <script type="text/javascript">
                var tinyMCE_object = <?php echo json_encode(
						array(
							'button_name'        => esc_html__( 'Add a video', 'mvf' ),
							'button_title'       => esc_html__( 'Why not add a video in your content?', 'mvf' ),
							'video_list'         => $this->video_list(),
							'image_title'        => esc_html__( 'Select video', 'mvf' ),
							'border_width_title' => esc_html__( 'Border width (in pixels)', 'mvf' ),
							'border_color_title' => esc_html__( 'Border color', 'mvf' ),
						)
					);
					?>;
            </script><?php
		}


		public function parse_shortcode( $atts ) {
			$attributes = shortcode_atts(
				array(
					'id'           => '',
					'border_color' => '#333',
					'border_width' => '8',
				),
				$atts,
				'mvf_video'
			);

			if ( is_admin() ) {
				return;
			}

			$post_id           = $attributes['id'];
			$video_id          = get_post_meta( $post_id, 'mvf-video-id', true );
			$video_title       = get_the_title( $post_id );
			$video_subtitle    = get_post_meta( $post_id, 'mvf-video-subtitle', true );
			$video_description = wp_trim_words( get_post_meta( $post_id, 'mvf-video-description', true ), 20 );
			$video_type        = strtolower( get_post_meta( $post_id, 'mvf-video-type', true ) );
			$border_color      = $attributes['border_color'];
			$border_width      = $attributes['border_width'];


			$css_output = ".video-{$post_id} {";
			$css_output .= "/* ooooooooooo */";
			$css_output .= "border: {$attributes['border_width']} solid {$attributes['border_color']}";
			$css_output .= "}";

			$this->css_output .= $css_output;


			//TODO: wp_add_inline_style() fails to work. Need to research why;


			switch ( $video_type ) {
				case 'dailymotion':
					$video_iframe .= "<iframe src='https://www.dailymotion.com/embed/video/{$video_id}'></iframe>";
					break;
				case 'youtube':
					$video_iframe .= "<iframe src='https://www.youtube.com/embed/{$video_id}' allow='autoplay; encrypted-media' allowfullscreen></iframe>";
					break;
				case 'vimeo':
					$video_iframe .= "<iframe src='https://player.vimeo.com/video/{$video_id}'></iframe>";
					break;
			}


			$html_output = "<div class='mvf-video-item video-{$post_id}' style='border: {$border_width}px solid {$border_color}'>";
			$html_output .= "<div class='mvf-video-item__video'>";
			$html_output .= $video_iframe;
			$html_output .= "</div>";
			$html_output .= "<div class='mvf-video-item__text'>";
			$html_output .= "<h3>{$video_title}</h3>";
			$html_output .= "<p class='subtitle'>{$video_subtitle}</p>";
			$html_output .= "<p>{$video_description}</p>";
			$html_output .= "</div>";
			$html_output .= "</div>";

			echo $html_output;
		}

		public static function add_assets() {
			wp_enqueue_style( 'mvf-video', MVF_PLUGIN_DIR . 'public/css/public.css' );
		}

	}

}