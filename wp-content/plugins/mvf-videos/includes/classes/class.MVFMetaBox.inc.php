<?php

if ( ! class_exists( 'MVF_Metabox' ) ) {
	class MVF_Metabox {

		public function __construct( $post_type_id, $metabox_id, $metabox_title, $metabox_context, $metabox_priority, $metabox_fields ) {
			$this->post_type_id     = $post_type_id;
			$this->metabox_id       = $metabox_id;
			$this->metabox_title    = $metabox_title;
			$this->metabox_context  = $metabox_context;
			$this->metabox_priority = $metabox_priority;
			$this->metabox_fields   = $metabox_fields;

			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'save_fields' ) );
		}

		public function post_types() {
			$metabox_post_types = array();
			if ( is_string( $this->post_type_id ) ) {
				$metabox_post_types[] = $this->post_type_id;
			} else {
				$metabox_post_types = $this->post_type_id;
			}

			return $metabox_post_types;
		}

		public function add_meta_boxes() {
			foreach ( $this->post_types() as $post_type ) {
				add_meta_box(
					$this->metabox_id,
					__( $this->metabox_title, 'mvf' ),
					array( $this, 'meta_box_callback' ),
					$post_type,
					$this->metabox_context,
					$this->metabox_priority
				);

			}
		}

		public function meta_box_callback( $post ) {
			wp_nonce_field( $post_type_id . '_custom_fields', $post_type_id . '_nonce' );
			$this->field_generator( $post );
		}

		public function field_generator( $post ) {
			$output = '';
			foreach ( $this->metabox_fields as $metabox_field ) {
				$label      = '<label for="' . $metabox_field['id'] . '">' . $metabox_field['label'] . '</label>';
				$meta_value = get_post_meta( $post->ID, $metabox_field['id'], true );
				if ( empty( $meta_value ) ) {
					$meta_value = $metabox_field['default'];
				}
				switch ( $metabox_field['type'] ) {
					case 'radio':
						$input = '<fieldset>';
						$input .= '<legend class="screen-reader-text">' . $metabox_field['label'] . '</legend>';
						$i     = 0;
						foreach ( $metabox_field['options'] as $key => $value ) {
							$metabox_field_value = ! is_numeric( $key ) ? $key : $value;
							$input               .= sprintf(
								'<label><input %s id=" %s" name="%s" type="radio" value="%s" > %s</label>%s',
								$meta_value === $metabox_field_value ? 'checked' : '',
								$metabox_field['id'],
								$metabox_field['id'],
								$metabox_field_value,
								$value,
								$i < count( $metabox_field['options'] ) - 1 ? '<br>' : ''
							);
							$i ++;
						}
						$input .= '</fieldset>';
						break;
					case 'wysiwyg':
						ob_start();
						wp_editor( $meta_value, $metabox_field['id'] );
						$input = ob_get_contents();
						ob_end_clean();
						break;
					default:
						$input = sprintf(
							'<input %s id="%s" name="%s" type="%s" placeholder="%s" value="%s" data-aaa>',
							$metabox_field['type'] !== 'color' ? 'style="width: 100%"' : '',
							$metabox_field['id'],
							$metabox_field['id'],
							$metabox_field['type'],
							$meta_value,
							! empty( $meta_value ) ? $meta_value : ''
						);
				}
				$output .= $this->format_rows( $label, $input );
			}
			echo '<table class="form-table"><tbody>' . $output . '</tbody></table>';
		}

		public function format_rows( $label, $input ) {
			return '<tr><th>' . $label . '</th><td>' . $input . '</td></tr>';
		}

		public function save_fields( $post_id ) {
			if ( ! isset( $_POST[ $post_type_id . '_nonce' ] ) ) {
				return $post_id;
			}
			$nonce = $_POST[ $post_type_id . '_nonce' ];
			if ( ! wp_verify_nonce( $nonce, $post_type_id . '_custom_fields' ) ) {
				return $post_id;
			}
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return $post_id;
			}
			foreach ( $this->metabox_fields as $metabox_field ) {
				if ( isset( $_POST[ $metabox_field['id'] ] ) ) {
					switch ( $metabox_field['type'] ) {
						case 'email':
							$_POST[ $metabox_field['id'] ] = sanitize_email( $_POST[ $metabox_field['id'] ] );
							break;
						case 'text':
							$_POST[ $metabox_field['id'] ] = sanitize_text_field( $_POST[ $metabox_field['id'] ] );
							break;
					}
					update_post_meta( $post_id, $metabox_field['id'], $_POST[ $metabox_field['id'] ] );
				} else if ( $metabox_field['type'] === 'checkbox' ) {
					update_post_meta( $post_id, $metabox_field['id'], '0' );
				}
			}
		}
	}
}