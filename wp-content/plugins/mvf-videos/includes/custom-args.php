<?php
$mfv_video_args = array(
	'supports'           => array( 'title' ),
	'publicly_queryable' => false
);


$mvf_video_fields = array(
	array(
		'label'   => 'Subtitle',
		'id'      => 'mvf-video-subtitle',
		'default' => 'Please add a subtitle',
		'type'    => 'text',
	),
	array(
		'label'   => 'Description',
		'id'      => 'mvf-video-description',
		'default' => 'Please add a description',
		'type'    => 'wysiwyg',
	),
	array(
		'label'   => 'Video ID',
		'id'      => 'mvf-video-id',
		'default' => 'Please add the ID of the video',
		'type'    => 'text',
	),
	array(
		'label'   => 'Type',
		'id'      => 'mvf-video-type',
		'type'    => 'radio',
		'options' => array(
			'Youtube',
			'Vimeo',
			'Dailymotion',
		),
	),
);

